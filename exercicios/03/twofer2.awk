#!/usr/bin/awk -f

BEGIN {
	for (i = 1; i < ARGC; i++) {
		if (ARGV[i] ~ /^[a-zA-Z_][a-zA-Z0-9_]*=.*/) { 
			Valid_Argument=1;
			continue;
		} else {
			print "Uma para voce, outra para mim"
		}
	}
}

Valid_Argument==1 {
	print "Uma para " $1 ", outra para mim"
}
