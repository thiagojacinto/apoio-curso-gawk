#!/usr/bin/awk -f

BEGIN {
	if (ARGC <= 1) {
		print "Um para voce, outro para mim"
		exit
	}
}

{
	print "Um para " $1 ", outro para mim"
}
