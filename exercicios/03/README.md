# Exercicio - Two fer

> _"Um para você, um para mim."_

Tentativas e uma solução para o problema proposto de "Two Fer"

## Solução

Apropriada a partir dos pensamentos do colega "gaeta". Ver script ["twofer_gaeta.awk"](https://codeberg.org/thiagojacinto/apoio-curso-gawk/src/branch/main/exercicios/03/twofer_gaeta.awk)
