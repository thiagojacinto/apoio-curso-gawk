#!/usr/bin/awk -f

BEGIN {
	if (ARGC==1 && !system("[ -t 0 ]")) {
		print "Um para voce, um para mim"
		exit
	}
}

{
	for (n = 1; n<= NF; n++)
		print "Um para " $1 ", um para mim"
}

ENDFILE {
	if (NR==0)
		print "Um para voce, um para mim"
}
