#!/usr/bin/awk -f

BEGIN { 
	IS_FILE=system("[ -e " $0 " ]")
	IS_PIPE=system("[ -p " $0 " ]")
	if (IS_PIPE==0 && IS_FILE>0) print "Uma para mim, outra para voce"; exit;
}

IS_FILE==0 {
	NOME=$1
	print "Uma para " $NOME ", outra para mim"
}
