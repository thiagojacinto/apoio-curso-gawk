#!/usr/bin/awk -f

{
	lista[$1] += 1 + INCREMENTO
}

ENDFILE {
	INCREMENTO += 1
}

END {
	for (item in lista) {
		if (lista[item] == 2) diferenca[item] = 1
		if (lista[item] == 3) intersecao[item] = 1
		if (lista[item] == 1 || lista[item] == 2) diferenca_simetrica[item] = 1
	}

	print "[UNIÃO]"
	for (item in lista) print item

	print "\n[DIFERENÇA]"
	for (item in diferenca) print item

	print "\n[INTERSEÇÃO]"
	for (item in intersecao) print item

	print "\n[DIFERENÇA SIMÉTRICA]"
	for (item in diferenca_simetrica) print item
}
