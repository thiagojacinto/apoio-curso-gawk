#!/usr/bin/awk -f

BEGIN {
	INCREMENTO = 0
}

{
	lista[$1] += 1 + INCREMENTO
}

ENDFILE {
	INCREMENTO += 1
}

END {
	print "[UNIÃO]"
	for (item in lista) {
		print item
	}
	
	print "\n[DIFERENÇA]"
	for (item in lista) {
		if (lista[item] == 2) {
			print item
		}
	}

	print "\n[INTERSEÇÃO]"
	for (item in lista) {
		if (lista[item] == 3) {
			print item
		}
	}

	print "\n[DIFERENÇA]"
	for (item in lista) {
		if (lista[item] == 1 || lista[item] == 2) {
			print item
		}
	}
}
