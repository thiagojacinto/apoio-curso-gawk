#!/usr/bin/awk -f
#
# Versão 2
#	- Implementa funcionalidade de usar script com outro arquivo formatado com chaves entre colchete.
#	- Uso: sec-data-v2.awk filtro1 ... filtroN [arquivo]
#

function is_file(filename) {
	return !system("[ -f '" filename  "' ]")
}

BEGIN {
	IGNORECASE=1
	if (ARGC > 2) imprimir_chave=1
	if (ARGC == 1) {
		erro=1
		exit
	}
	for (indice = ARGC; indice > 1; indice--) {
		if (ARGV[indice-1] ~ /\[|\]/) {
			erro=1
			exit
		}
		ARGV[indice]=ARGV[indice-1]
	}
	ARGC++
	ARGV[1]="/home/thiago/curso-gawk/exercicios/04/ex4-sec-dados/dados"

	# caso seja usado outro arquivo no último parâmetro.
	if (is_file(ARGV[ARGC-1])) {
		ARGV[1] = ARGV[ARGC-1]
		ARGC--
		if (ARGC <= 3) imprimir_chave=0
	}
}

/\[.*?\]/ {
	for (indice = ARGC-1; indice > 1; indice--) {
		if ($0 ~ ARGV[indice]) {
			ocorrencia=1
			iniciar_impressao=1
			argumento_ativado[indice]++
			if (imprimir_chave) print $0
			next
		}
	}
	iniciar_impressao=0
}

NF && iniciar_impressao 	# NF para evitar impressão de linhas vazias

ENDFILE { exit }

END {
	if (erro) {
		print "Uso: " ENVIRON["_"] " filtro1 ... filtroN [arquivo]"
		print "		filtro não pode conter colchetes: []"
		print "		- opcional: informar arquivo para ser filtrado"
		exit 1
	}
	if (!ocorrencia) {
		print "Erro: sem correspondência com nenhum dos argumentos"
		exit 1
	}
	for (indice = ARGC-1; indice > 1; indice--)
		if (!argumento_ativado[indice]) print " > Sem correspondência com argumento: " ARGV[indice]
}
