#!/usr/bin/awk -f

BEGIN {
	IGNORECASE=1
	if (ARGC > 2) imprimir_chave=1
	if (ARGC == 1) {
		erro=1
		exit
	}
	for (indice = ARGC; indice > 1; indice--) {
		if (ARGV[indice-1] ~ /\[|\]/) {
			erro=1
			exit
		}
		ARGV[indice]=ARGV[indice-1]
	}
	ARGC++
	ARGV[1]="/home/thiago/curso-gawk/exercicios/04/ex4-sec-dados/dados"
}

/\[.*?\]/ {
	for (indice = ARGC-1; indice > 1; indice--) {
		if ($0 ~ ARGV[indice]) {
			ocorrencia=1
			iniciar_impressao=1
			argumento_ativado[indice]++
			if (imprimir_chave) print $0
			next
		}
	}
	iniciar_impressao=0
}

NF && iniciar_impressao 	# NF para evitar impressão de linhas vazias

ENDFILE { exit }

END {
	if (erro) {
		print "Uso: " ENVIRON["_"] " filtro1 ... filtroN"
		print "		filtro não pode conter colchetes: []"
		exit 1
	}
	if (!ocorrencia) {
		print "Erro: sem correspondência com nenhum dos argumentos"
		exit 1
	}
	for (indice = ARGC-1; indice > 1; indice--)
		if (!argumento_ativado[indice]) print " > Sem correspondência com argumento: " ARGV[indice]
}
