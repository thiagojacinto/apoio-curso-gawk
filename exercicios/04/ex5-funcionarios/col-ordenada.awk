#!/usr/bin/awk -f

BEGIN {
#    if (ARGC < 2) erro=1; exit
    if (ARGC < 2) {
        ARGV[1] ="funcionarios.data"
        ARGV[2] ="nome"
        ARGC    =ARGC+2
    } else {
        ARGV[2] = ARGV[1]
        ARGV[1] = "funcionarios.data"
    }
    # print ARGV[1], ARGV[2]
    
    if (ARGV[2] !~ /nome|idade|cargo|salario/) {
        erro=2
        exit
    }

    RS=""
    FS="\n"
    
    coluna=ARGV[2]
}

{
    switch (coluna) {
        case "idade":
            indice_coluna = $2
            break
        case "cargo":
            indice_coluna = $3
            break
        case "salario":
            indice_coluna = $4
            break
        default:
            indice_coluna = $1
            break
    }

    nome[indice_coluna]    = $1
    idade[indice_coluna]   = $2
    cargo[indice_coluna]   = $3
    salario[indice_coluna] = $4
    #print $0
    #print ">>> NR="NR"\n",  $1, $2, $3, $4
}

ENDFILE { exit }

END {
    if (erro) {
        print "Uso: " ENVIRON["_"] " coluna"
        print "     - 'coluna' poderá ser: nome, idade, cargo ou salario"
        exit erro
    }
    
    switch (coluna) {
        case "idade":
            asort(idade, ordenada)
            break
        case "cargo":
            asort(cargo, ordenada)
            break
        case "salario":
            asort(salario, ordenada)
            break
        default:
            asort(nome, ordenada)
            break
    }

    formato = "| %-18s | %5s | %-13s | %-7.2f |\n"
    printf "| %-18s | %-5s | %-13s | %-7s |\n" , "NOME", "IDADE", "CARGO", "SALARIO"
    for (indice in ordenada) {
        printf formato, nome[ordenada[indice]], idade[ordenada[indice]], cargo[ordenada[indice]], salario[ordenada[indice]]
    }
}
