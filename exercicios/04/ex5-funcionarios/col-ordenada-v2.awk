#!/usr/bin/awk -f

function cmp_field(i1, v1, i2, v2)
{
    # comparison by value, as string, and ascending order
    return v1[POS] < v2[POS] ? -1 : (v1[POS] != v2[POS])
}

BEGIN {
    if (ARGC < 2) {
        ARGV[1] ="funcionarios.data"
        ARGV[2] ="nome"
        ARGC    =ARGC+2
    } else {
        ARGV[2] = ARGV[1]
        ARGV[1] = "funcionarios.data"
        if (ARGC > 2)
            print "\n >>>> [INFO] A ordenação acontecerá apenas para o primeiro argumento: " ARGV[2] "\n"
        
    }
    
    if (ARGV[2] !~ /nome|idade|cargo|salario/) {
        erro=2
        exit
    }

    RS=""
    FS="\n"
    
    coluna=ARGV[2]
    switch(coluna) {
        case "idade":
            POS=2
            break
        case "cargo":
            POS=3
            break
        case "salario":
            POS=4
            break
        default:
            POS=1
            break
    }
}

{
    for (indice = 1; indice <= NF; indice++)
        lista[NR][indice] = $indice
}

ENDFILE { exit }

END {
    if (erro) {
        print "Uso: " ENVIRON["_"] " coluna"
        print "     - 'coluna' poderá ser: nome, idade, cargo ou salario"
        exit erro
    }

    PROCINFO["sorted_in"] = "cmp_field"

    printf "%s,%s,%s,%s\n" , "NOME", "IDADE", "CARGO", "SALARIO"
    for (indice in lista) {
        for (j = 1; j <= NF; j++)
        printf("%s%c", lista[indice][j], j < NF ? "," : "\n")
    }
}
