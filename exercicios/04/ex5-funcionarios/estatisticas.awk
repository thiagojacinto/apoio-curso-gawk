#!/usr/bin/awk -f

function exibir_em_tela(titulo, dado, tipo) {
	espaco=22
    switch (tipo) {
		case "inteiro":
			printf "%-*s: %02d \n",espaco, titulo, dado
            break
		case "dinheiro":
			printf "%-*s: R$ %'.2f\n",espaco, titulo, dado
		    break
        default:
			printf "%-*s: %s\n",espaco, titulo, dado
	        break
    }
}

BEGIN {
	IGNORECASE=1
	FS="\n"
	#RS="[[:blank:]]"
    RS=""

	coluna[1]="Número de funcionários"
	coluna[2]="Secretárias(os)"
	coluna[3]="Funcionário mais velho"
	coluna[4]="Funcionário mais novo"
	coluna[5]="Menor salário"
	coluna[6]="Maior salário"
}

BEGINFILE {
    numero_de_funcionarios = 0
    numero_de_secretarios = 0
    max_salario = 0
    min_salario = 999999999
    max_idade = 0
    min_idade = 130
    mais_velho = ""
    mais_novo = ""
}

{
	numero_de_funcionarios++
	if ($3 ~ /secret/) numero_de_secretarios++
    if ($4 >= 0) {
        if ($4 > max_salario) max_salario = $4
        if ($4 < min_salario) min_salario = $4
    }
    if ($2 > max_idade) { 
        max_idade = $2
        mais_velho = $1
    }
    if ($2 < min_idade) {
        min_idade = $2
        mais_novo = $1
    }
}

ENDFILE {
	printf "\n[Estatísticas de '%s']\n", FILENAME 
    exibir_em_tela(coluna[1], numero_de_funcionarios,"inteiro")
    exibir_em_tela(coluna[2], numero_de_secretarios ,"inteiro")
    exibir_em_tela(coluna[3], mais_velho)
    exibir_em_tela(coluna[4], mais_novo)
    exibir_em_tela(coluna[5], min_salario, "dinheiro")
    exibir_em_tela(coluna[6], max_salario, "dinheiro")
}
