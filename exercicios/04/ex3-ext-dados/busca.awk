#!/usr/bin/awk -f

BEGIN  { 
	FIELDWIDTHS = "19 9 9"
	item=ARGV[1]
	ARGV[1] = "/home/thiago/curso-gawk/exercicios/04/ex3-ext-dados/produtos"
}

NR > 2 {
	if (tolower($1) ~ tolower(item)) {

		origem = "        Sem origem"
		if ($2 ~ /x/) {
			origem = "Nacional"
		}
		if ($3 ~ /x/) {
			origem = "Importado"
		}
		print $1 "\t" origem
		foi_encontrado=1
	}
}

END {
	if (!foi_encontrado) print item ": nenhum item encontrado"
}
