#!/usr/bin/awk -f

BEGIN  { FIELDWIDTHS = "19 9 9" }

NR > 2 {
	if ($2 ~ /x/) {
		nacionais += 1
	}
	if ($3 ~ /x/) {
		importados += 1
	}
	if ($2 !~ /x/ && $3 !~ /x/) {
		desconhecidos += 1
	}
}

END {
	print "Produtos nacionais      :", nacionais
	print "Produtos importados     :", importados
	print "Procedência desconhecida:", desconhecidos
}
