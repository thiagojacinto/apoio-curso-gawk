function display_help() {
    printf "\n%s\n", "USO:"
    print " usrinfo [-f='LISTA,DE,CAMPOS'] [NOME_DE_LOGIN]"
    print " CAMPOS VÁLIDOS:"
    formato_opcao = "\t* %s\t-\t%s\n"
    printf formato_opcao, "login","Nome de login"
    printf formato_opcao, "name","Nome completo"
    printf formato_opcao, "uid","ID do usuário"
    printf formato_opcao, "gid","ID do grupo"
    printf formato_opcao, "home","Diretório home do usuário"
    printf formato_opcao, "shell","Shell definido para o usuário"
    printf formato_opcao, "all","Todos os campos na ordem em /etc/passwd"
}

function name_simplifier(input_name) {
    return gensub(/,+/, " ", "g", input_name)
}

BEGIN {
    # Teste dos argumentos
    #print "[DEBUG] ", list, user, error
    IGNORECASE=1 
    
    if (user == 0) {
        print "Usuário não existe!"
        display_help()
        exit
    }

    if (error) {
        switch (error) {
            case 1:
                print "Paramêtros incorretos!"
                break
            case 2:
                print "Número incorreto de parâmetros!"
                break
            default:
                print "Verifique os parâmetros utilizados."
                break
        }
        display_help()
        exit
    }

    # Arquivo de dados
    ARGV[1]="/etc/passwd"
    ARGC=2
    FS=":"
    SPECIFIC_USER="^"user"$"
}

list != "all" && $1 ~ SPECIFIC_USER {
    formato_opcoes = "%s "
    if (list ~ "login") printf formato_opcoes, $1
    if (list ~ "name")  printf formato_opcoes, name_simplifier($5)
    if (list ~ "uid")   printf formato_opcoes, $3
    if (list ~ "gid")   printf formato_opcoes, $4
    if (list ~ "home")  printf formato_opcoes, $6
    if (list ~ "shell") printf formato_opcoes, $7
    print ""
    exit
}

$1 ~ SPECIFIC_USER {
    formato_usuario = "%-5s: %s\n"
    printf formato_usuario,"Login", $1
    printf formato_usuario,"Nome", name_simplifier($5)
    printf formato_usuario,"UID", $3
    printf formato_usuario,"GID", $4
    printf formato_usuario,"Home", $6
    printf formato_usuario,"Shell", $7
} 
