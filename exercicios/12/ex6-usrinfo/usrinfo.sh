#!/usr/bin/env bash

# Parser das opções do usrinfo.awk

is_list() [[ ${1::3} = '-f=' ]]

is_user() { id -u $1 &>/dev/null; }

get_list() {
    local l=${1:3}
    [[ $l ]] && echo $l || return 1
}

case $# in
    0)  # Usuário corrente e todos os dados...
        opts="all $USER 0"
        ;;
    1)  # Pode ser lista ou usuário...
        if is_list $1; then
            l=$(get_list $1) && e=0 || e=1
            opts="${l:-0} $USER ${e:-0}"
        elif is_user $1; then
            opts="all $1 0"
        else
            opts="0 0 1"
        fi
        ;;
    2)  # Tem que ser lista-usuário ou usuário-lista...
        if is_list $1 && is_user $2; then
            l=$(get_list $1) || e=1
            opts="${l:-0} $2 ${e:-0}"
        elif is_user $1 && is_list $2; then
            l=$(get_list $2) || e=1
            opts="${l:-0} $1 ${e:-0}"
        else
            opts="0 0 1"
        fi
        ;;
    *)  # Número incorreto de argumentos...
        opts="0 0 2"
        ;;
esac

read list user error <<< $opts

awk -f usrinfo.awk -v list=$list -v user=$user -v error=$error
