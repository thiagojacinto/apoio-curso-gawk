#!/usr/bin/awk -f

BEGIN {
	print ">>>>>>>> Ola, mundo!"
	FS="/" 
}

ENDFILE {
	FS=":"
	print " >>>> File name: "  FILENAME
}

{
	print $NF
}

END {
	print "<<<<<<<< Acabei o processamento!"
}
