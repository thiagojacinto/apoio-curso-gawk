# apoio-curso-gawk

Conteúdo de apoio para curso de [GAWK](https://debxp.org/programando-em-awk/)

## Atalhos

> [exercicios](exercicios)   
>> [01](exercicios/01)   
>>> [catalogo.csv](exercicios/01/catalogo.csv)   
>>
>> [03](exercicios/03)   
>>> [arquivo-vazio](exercicios/03/arquivo-vazio)   
>>> [README.md](exercicios/03/README.md)   
>>> [texto.txt](exercicios/03/texto.txt)   
>>> [twofer](exercicios/03/twofer)   
>>> [twofer2.awk](exercicios/03/twofer2.awk)   
>>> [twofer3.awk](exercicios/03/twofer3.awk)   
>>> [twofer.awk](exercicios/03/twofer.awk)   
>>> [twofer_gaeta.awk](exercicios/03/twofer_gaeta.awk)   
>>
>> [04](exercicios/04)   
>>> [ex2-conjuntos](exercicios/04/ex2-conjuntos)   
>>>> [conjuntos.awk](exercicios/04/ex2-conjuntos/conjuntos.awk)   
>>>> [conjuntos_v1.awk](exercicios/04/ex2-conjuntos/conjuntos_v1.awk)   
>>>> [lista1.txt](exercicios/04/ex2-conjuntos/lista1.txt)   
>>>> [lista2.txt](exercicios/04/ex2-conjuntos/lista2.txt)   
>>>
>>> [ex3-ext-dados](exercicios/04/ex3-ext-dados)   
>>>> [busca.awk](exercicios/04/ex3-ext-dados/busca.awk)   
>>>> [produtos](exercicios/04/ex3-ext-dados/produtos)   
>>>> [resumo.awk](exercicios/04/ex3-ext-dados/resumo.awk)   
>>>
>>> [ex4-sec-dados](exercicios/04/ex4-sec-dados)   
>>>> [dados](exercicios/04/ex4-sec-dados/dados)   
>>>> [exemplos](exercicios/04/ex4-sec-dados/exemplos)   
>>>>> [Cimplicity.ini](exercicios/04/ex4-sec-dados/exemplos/Cimplicity.ini)   
>>>>> [Customers.ini](exercicios/04/ex4-sec-dados/exemplos/Customers.ini)   
>>>>
>>>> [sec-data.awk](exercicios/04/ex4-sec-dados/sec-data.awk)   
>>>> [sec-data-v2.awk](exercicios/04/ex4-sec-dados/sec-data-v2.awk)   
>>>
>>> [ex5-funcionarios](exercicios/04/ex5-funcionarios)   
>>>> [col-ordenada.awk](exercicios/04/ex5-funcionarios/col-ordenada.awk)   
>>>> [col-ordenada-v2.awk](exercicios/04/ex5-funcionarios/col-ordenada-v2.awk)   
>>>> [estatisticas.awk](exercicios/04/ex5-funcionarios/estatisticas.awk)   
>>>> [funcionarios2.data](exercicios/04/ex5-funcionarios/funcionarios2.data)   
>>>> [funcionarios.data](exercicios/04/ex5-funcionarios/funcionarios.data)   
>>
>> [12](exercicios/12)   
>>> [ex6-usrinfo](exercicios/12/ex6-usrinfo)   
>>>> [usrinfo.awk](exercicios/12/ex6-usrinfo/usrinfo.awk)   
>>>> [usrinfo.sh](exercicios/12/ex6-usrinfo/usrinfo.sh)   

Fonte:
- [GitMarkdownTree.js](https://gist.github.com/Skitionek/9db395016195864aa18efac5e312771f) source code